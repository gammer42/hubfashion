<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $fillable = ['name','category_id','brand_id','slug','ideal','size','fabric','color','price','discount','description','status'];


    public function categories(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function brands(){
        return $this->belongsTo(Brand::class,'brand_id','id');
    }

    public function photos(){
        return $this->hasMany(Photo::class,'foreign_id','id');
    }
}
