<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Category;


class Brand extends Model
{

    protected $table = 'brands';
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $fillable = ['name','slug','status'];



    public function products(){
        return $this->hasMany(Product::class,'brand_id','id');
    }


    public function categories(){
        return $this->belongsToMany(Category::class,'brand_category','brand_id');
    }
}
