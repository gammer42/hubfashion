<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = ['foreign_id', 'org_image', 'thumbnail'];


    public function products(){
        return $this->belongsTo(Product::class);
    }

    public function categories(){
        return $this->belongsTo(Category::class,'foreign_id','id');
    }
}