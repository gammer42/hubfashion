<?php

namespace App\Http\Controllers;

use Image;
use App\Brand;
use App\Photo;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Psr\Log\NullLogger;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::with('children')->where('parent_id','=',0)->orderBy('name', 'asc')->get();
        $brands  = Brand::all();
        return view('admin.products.create', compact('categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'category' => 'required',
            'brand' => 'required',
            'slug' => 'required|unique:products,slug',
            'ideal' => 'required',
            'size' => 'required',
            'fabric' => 'required',
            'color' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'quantity' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);
        $store = new Product();

        $store->name        =   $request->name;
        $store->category_id =   $request->category;
        $store->brand_id    =   $request->brand;
        $store->slug        =   $request->slug;
        $store->ideal       =   $request->ideal;
        $store->size       =   $request->size;
        $store->fabric      =   $request->fabric;
        $store->color       =   $request->color;
        $store->price       =   $request->price;
        $store->discount    =   $request->discount;
        $store->quantity    =   $request->quantity;
        $store->description =   $request->description;

        $store->save();

        if($request->hasFile('image')) {
            foreach ($request->file('image') as $item) {

                $img = new Photo();

                $img->image_type = 0; // For Product Type
                $img->foreign_id = $store->id; //Add foreign Key

                $originalImage = 'product-'. str_random(5).rand(0000,9999);
                $originalImage = $originalImage . '.' .$item->getClientOriginalExtension();
                $image = $item;

                $image_name = $this->saveImage($image,$originalImage, NULL);

                $img->org_image = $image_name;
                $img->thumbnail = 'thumbnail-'.$image_name;
                $img->save();
            }
        }
            Session::flash('message','Product Added Successfully!!');
        return redirect()->route('products.index');
    }


    public function saveImage($image, $image_name,$oldImage = NULL)
    {
        $image_path = public_path('images/products/');
        Image::make($image->getRealPath())->interlace()->save($image_path . $image_name, 80);
        if (!is_null($oldImage)){
            File::delete($image_path . '/' . $oldImage);
        }
        $thumbnail_path = public_path('images/thumbnails/');
        Image::make($image->getRealPath())->resize(200,150)->interlace()->save($thumbnail_path . 'thumbnail-'.$image_name, 80);
        if (!is_null($oldImage)){
            File::delete($image_path . '/' . $oldImage);
        }
        return $image_name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::findOrFail($id)->with('categories','brands')->first();
        $images = Photo::query()->where('image_type','=',0)->where('foreign_id','=',$id)->get();
        return view('admin.products.show', compact('products','images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::findOrFail($id)->with('categories','brands')->first();
        // dd($products);
        $categories = Category::with('children')->where('parent_id','=',0)->orderBy('name', 'asc')->get();
        $brands = Brand::where('status',1)->get();
        return view('admin.products.edit', compact('products','categories','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'category' => 'required',
            'brand' => 'required',
            // 'slug' => 'required|unique:products,slug',
            'ideal' => 'required',
            'size' => 'required',
            'fabric' => 'required',
            'color' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'description' => 'required'
        ]);

        $store = Product::find($id);

        $store->name        =   $request->name;
        $store->category_id =   $request->category;
        $store->brand_id    =   $request->brand;
        $store->slug        =   $request->slug;
        $store->ideal       =   $request->ideal;
        $store->size       =   $request->size;
        $store->fabric      =   $request->fabric;
        $store->color       =   $request->color;
        $store->price       =   $request->price;
        $store->discount    =   $request->discount;
        $store->description =   $request->description;

        $store->save();

        Session::flash('message','Product Added Successfully!!');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();

        Session::flash('message','Product Deleted!!!');
        return redirect()->route('products.index');
    }

    public function publish($id)
    {
        $products = Product::find($id);

        $products->status = 1;
        $products->save();

        Session::flash('message','Product Published!!!');
        return redirect()->back();
    }

    public function unpublish($id)
    {
        $products = Product::find($id);

        $products->status = 0;
        $products->save();

        Session::flash('message','Product Unpublished!!!');
        return redirect()->back();
    }
}
