<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index()
    {
        $mens_products = array();
        $womens_products = array();
        $boys_products = array();
        $girls_products = array();

        $mens = Category::where('parent_id','=',1)->where('status','=',1)->get()->toArray();
            $i = 0;
        foreach ($mens as $men) {
            $temp = Product::with('photos')->where('category_id','=', $men['id'])->first();
            if(isset($temp)) {
                $mens_products[$i] = $temp;
                $i++;
            }
        }

        $womens = Category::query()->where('parent_id','=',2)->where('status','=',1)->get()->toArray();
        foreach ($womens as $women) {
            $temp = Product::with('photos')->where('category_id','=', $women['id'])->first();
            if(isset($temp)) {
                $womens_products[$i] = $temp;
                $i++;
            }
        }

        $girls = Category::query()->where('parent_id','=',3)->where('status','=',1)->get()->toArray();
        foreach ($girls as $girl) {
            $temp = Product::with('photos')->where('category_id','=', $girl['id'])->first();
            if(isset($temp)) {
                $girls_products[$i] = $temp;
                $i++;
            }
        }

        $boys = Category::query()->where('parent_id','=',4)->where('status','=',1)->get()->toArray();
        foreach ($boys as $boy) {
            $temp = Product::with('photos')->where('category_id','=', $boy['id'])->first();
            if(isset($temp)) {
                $boys_products[$i] = $temp;
                $i++;
            }
        }

//        dd($mens[0]->slug);
        return view('frontend.index',compact('mens', 'womens', 'girls', 'boys', 'mens_products', 'womens_products', 'girls_products', 'boys_products'));
    }

    public function registration()
    {
        return view('frontend.register');
    }

    public function login()
    {
        return view('frontend.login');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function blog()
    {
        return view('frontend.blog');
    }

    public function boy()
    {
        return view('frontend.boy');
    }

    public function boys()
    {
        return view('frontend.boys');
    }

    public function checkout()
    {
        return view('frontend.checkout');
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function faq()
    {
        return view('frontend.faq');
    }

    public function girl()
    {
        return view('frontend.girl');
    }

    public function girls()
    {
        return view('frontend.girls');
    }

    public function men()
    {
        return view('frontend.men');
    }

    public function mens(Request $id)
    {
        $mens = Category::where('parent_id','=',1)->where('status','=',1)->get()->toArray();
        $womens = Category::query()->where('parent_id','=',2)->where('status','=',1)->get()->toArray();
        $boys = Category::query()->where('parent_id','=',4)->where('status','=',1)->get()->toArray();
        $girls = Category::query()->where('parent_id','=',3)->where('status','=',1)->get()->toArray();

        $products = Product::query()->with('photos')->where('category_id','=',$id)->get();

        $i = 0;
        $mens_products = array();
        foreach ($mens as $men) {
            $temp = Product::with('photos')->where('category_id','=', $men['id'])->get();
            $j = 0;
            if(isset($temp)) {
                $mens_products[$i] = $temp;
                $i++;
            }
        }

        return view('frontend.mens', compact('mens', 'womens', 'boys', 'girls','products', 'mens_products'));
    }

    public function payment()
    {
        return view('frontend.payment');
    }

    public function shop()
    {
        return view('frontend.shop');
    }

    public function blog_single()
    {
        return view('frontend.single');
    }

    public function women(Request $slug)
    {
        $products = Product::query()->where('slug','=', $slug)->first();
        return view('frontend.women',compact('products'));
    }

    public function womens()
    {
        return view('frontend.womens');
    }

    public function kids()
    {
        return redirect()->back();
    }

}
