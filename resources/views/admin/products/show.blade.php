@extends('layouts.admin')
@section('navbar')
    <a class="navbar-brand bold" href="#"><h3 class="bold">{{ $products->name }}</h3></a>
@endsection
@section('content')

    <div class="wrapper ">

        @include('admin.partial.sidebar')

        <div class="main-panel">
            <!-- Navbar -->
        @include('admin.partial.nav')


        <!--  User Details -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-success">
                                    @if(session('message'))
                                        <h3 style="text-align: center; color:snow; font-weight: bold;" class="card-title mt-0">{{session('message')}}</h3>
                                    @endif
                                </div>
                                <div style="display: flex" class="card-header card-header-primary show-products-header">
                                    <span class="bold" id="stock" style="width:300px;"><h4 class="card-title bold"><span class="card-title bold">{{ $products->quantity }}</span>  Products are In Stock</h4></span>
                                    @if($products->status == 1)
                                    <a class="" id="" href="{{ route('products.unpublish',$products->id) }}"><button class="btn btn-warning pull-right">Unpublish</button></a>
                                    @elseif($products->status == 0)
                                    <a class="" id="" href="{{ route('products.publish',$products->id) }}"><button class="btn btn-warning pull-right">Publish Product</button></a>
                                    @endif
                                    <form action="{{ route('products.destroy',$products->id) }}" method="POST">
                                        @CSRF
                                        @METHOD('DELETE')
                                        <button type="submit" class="btn btn-danger pull-right" onclick="return confirmDel()" title="Delete Product">Delete</button>
                                    </form>
                                </div>
                                <div class="card-body" >
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">1. Product Name</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white" >{{ $products->name }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">2. Category</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->categories->name }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">3. Brand</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->brands->name }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">4. Slug</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="bmd-label-floating bold white">{{ $products->slug }}</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">5. Ideals</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->ideal }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">6. Size</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    @if($products->size == 'XS')
                                                        <label class="bmd-label-floating bold white">Extra Small (XS)</label>
                                                    @elseif($products->size == 'S')
                                                        <label class="bmd-label-floating bold white">Small (S)</label>
                                                    @elseif($products->size == 'M')
                                                        <label class="bmd-label-floating bold white">Medium (M)</label>
                                                    @elseif($products->size == 'L')
                                                        <label class="bmd-label-floating bold white">Long (L)</label>
                                                    @elseif($products->size == 'XL')
                                                        <label class="bmd-label-floating bold white">Extra Long (XL)</label>
                                                    @elseif($products->size == 'XXL')
                                                        <label class="bmd-label-floating bold white">Double Extra Long (XXL)</label>
                                                    @elseif($products->size == 'XXXL')
                                                        <label class="bmd-label-floating bold white">Very Long (XXXL)</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">7. Fabric</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->fabric }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">8. Status</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->status = 1? "Active":"Inactive" }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">9. Color</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->color }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-2"></div>--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label class="bmd-label-floating bold">Image</label>--}}
                                                    {{--<label class="bmd-label-floating bold white" style="float:right;">:</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-5">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label class="bmd-label-floating bold white">{{ $products->image[] }}</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">10. Price TK</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->price }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">11. Discount (%)</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->discount }} %</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">12. Description</label>
                                                    <label class="bmd-label-floating bold white" style="float:right;">:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold white">{{ $products->description }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="image1" class="text-center">
                                <div id="image2" class="fileinput-preview fileinput-exists thumbnail img-raised">
                                    @foreach($images as $image)
                                        <div class="photoinfo">
                                        <img src="{{ url('/images/products',$image->org_image) }}">
                                            <form action="{{ route('photos.destroy',$image->id) }}" method="POST">
                                                @CSRF
                                                @METHOD('DELETE')
                                                <input type="hidden" name="pro" value="{{ $products->id }}"/>
                                            <button type="submit" style="top: -40px !important; padding: 5px 10px !important; margin: 0px 15px !important;" class="btn btn-danger btn-round pull-right dl btn-sm" title="Delete Image" onclick="confirmDel()">Delete image</button>
                                            </form>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            @include('admin.partial.footer')
        </div>
    </div>

    {{--@include('admin.partial.plugin')--}}

    @include('admin.partial.script')
@endsection

@section('script')
    <script>
        function confirmDel() {
            let del = confirm('Are you sure to delete?');
            if(del)
                return true;
            else
                return false;
        }
        const x = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = '&copy; ' + x + date.innerHTML;

        function checkPermission() {
            let checkBox = document.getElementById("checkBox");
            let permissionBox = document.getElementById("permissionBox");
            if (checkBox.checked !== true){
                permissionBox.style.display = "block";
            } else {
                permissionBox.style.display = "none";
            }
        }

        function toggle(source) {
            let checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (let i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] !== source)
                    checkboxes[i].checked = source.checked;
            }
        }

        $("#product_name").keyup(function(){
            let text = $(this).val();
            // let parslug = $('#parent').find(":selected").text();
            // // let text = $('#category_name').val();
            // let slug = custom_str_concate(parslug,text);
            // slug = slug.trim();
            slug = text.trim();
            slug = slug.toLowerCase().replace(/ /g, '-').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
            $("#slug_name").val(slug);
        });

        function custom_str_concate(val1,val2){
            if(val1 === "/")
                return '_'+val1.concat(val2);
            else
                return val1+'-'+val2;
        }
        $(document).ready(function(){
            $("select#parent").change(function(){
                let val1 = $('#parent').find(":selected").text();
                let val2 = $('#category_name').val();
                let slug = custom_str_concate(val1,val2);
                slug = slug.trim();
                slug = slug.toLowerCase().replace(/ /g, '_').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
                $("#slug_name").val(slug);
            });
        });
        document.forms['edit_product_form'].elements['size'].value = "{{ $products->size }}";
        document.forms['edit_product_form'].elements['brand'].value = "{{ $products->brand_id }}";
        document.forms['edit_product_form'].elements['ideal'].value = "{{ $products->ideal }}";
        document.forms['edit_product_form'].elements['category'].value = "{{ $products->category_id }}";
    </script>



@endsection
























































<!--

{{--@extends('layouts.admin')--}}

{{--@section('head')--}}
{{--<style>--}}
    {{--#table_id_length,--}}
    {{--#table_id_filter,--}}
    {{--#table_id_info,--}}
    {{--#table_id_paginate{--}}
        {{--display:none;--}}
    {{--}--}}

{{--</style>--}}
{{--@endsection--}}

{{--@section('navbar')--}}
    {{--<a class="navbar-brand" href="#" title="View Customer Users table">Products</a>--}}
{{--@endsection--}}

{{--@section('content')--}}

    {{--<div class="wrapper ">--}}

        {{--@include('admin.partial.sidebar')--}}


        {{--<div class="main-panel">--}}
            {{--<!-- Navbar -->--}}
        {{--@include('admin.partial.nav')--}}
        {{--<!-- End Navbar -->--}}

            {{--<!-- User List Table  -->--}}

            {{--<div class="content">--}}
                {{--<div class="container-fluid">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12" id="admin" style="display: block;">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-header card-header-success">--}}
                                    {{--@if(session('message'))--}}
                                        {{--<h3 style="text-align: center; color:snow; font-weight: bold;" class="card-title">{{session('message')}}</h3>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="card-header card-header-primary">--}}
                                    {{--<div class="col-md-4 "style="float:left;">--}}
                                        {{--<h4 class="card-title ">Products</h4>--}}
                                        {{--<p class="card-category">Products Table</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4" style="float:left;">--}}
                                        {{--<h4 class="card-title"><a  class="nav-link navbar-brand" href="{{route('products.create')}}"  title="Create New Product">{{ __('Create New Product') }}</a></h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="card-body">--}}
                                    {{--<div class="table-responsive">--}}
                                        {{--<table class="table" id="table_id">--}}
                                            {{--<!-- <thead class=" text-primary"> -->--}}
                                            {{--<thead class=" text-primary">--}}
                                            {{--<th style="width: 50px;">--}}
                                                {{--ID--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 100px;">--}}
                                                {{--Name--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 100px;">--}}
                                                {{--Slug--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 80px;">--}}
                                                {{--Ideal--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 80px;">--}}
                                                {{--Size--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 80px;">--}}
                                                {{--Fabric--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 80px;">--}}
                                                {{--Color--}}
                                            {{--</th>--}}
                                            {{--<th style="width: 80px;">--}}
                                                {{--Price--}}
                                            {{--</th>--}}
                                            {{--<th style="">--}}
                                                {{--Description--}}
                                            {{--</th>--}}
                                            {{--<th class="action-btn" style="width: 80px; float:right;">--}}
                                                {{--Action--}}
                                            {{--</th>--}}
                                            {{--</thead>--}}
                                            {{--<tbody>--}}
                                                {{--<tr style="background:#202940;">--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->id }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->name }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->slug }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->ideal }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->size == 'XS'? "Extra Small (XS)":"" }}--}}
                                                        {{--{{ $products->size == 'S'? "Small (S)":"" }}--}}
                                                        {{--{{ $products->size == 'M'? "Medium (M)":"" }}--}}
                                                        {{--{{ $products->size == 'L'? "Long (L)":"" }}--}}
                                                        {{--{{ $products->size == 'XL'? "Extra Long (XL)":"" }}--}}
                                                        {{--{{ $products->size == 'XXL'? "Double Extra Long (XXL)":"" }}--}}
                                                        {{--{{ $products->size == 'XXXL'? "Very Long (XXXL)":"" }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->fabric }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->color }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->price }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="white">--}}
                                                        {{--{{ $products->description }}--}}
                                                    {{--</td>--}}
                                                    {{--<td class="center white">--}}
                                                        {{--<a class="m-btn btn btn-info" href="{{route('products.edit',$products->id)}}">--}}
                                                            {{--<i class="material-icons">settings</i>--}}
                                                        {{--</a>--}}
                                                        {{--<form style="display: inline !important;" action="{{route('products.destroy', $products->id)}}" method="POST">--}}
                                                            {{--@CSRF--}}
                                                            {{--@METHOD('DELETE')--}}
                                                            {{--<button type="submit" class="m-btn btn btn-danger" title="Delete Product" onclick="return confirmDel()">--}}
                                                                {{--<i class="material-icons">delete</i>--}}
                                                            {{--</button>--}}
                                                        {{--</form>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


        {{--<!--  Footer  -->--}}

        {{--@include('admin.partial.footer')--}}

    {{--</div>--}}
    {{--</div>--}}


{{--@endsection--}}

{{--@section('script')--}}

    {{--<script>--}}
        {{--const x = new Date().getFullYear();--}}
        {{--let date = document.getElementById('date');--}}
        {{--date.innerHTML = '&copy; ' + x + date.innerHTML;--}}
    {{--</script>--}}

    {{--<script>--}}
        {{--function toggle_visibility(id1,id2,id3,id4) {--}}
            {{--let a = document.getElementById(id1);--}}
            {{--let u = document.getElementById(id2);--}}
            {{--let ab = document.getElementById(id3);--}}
            {{--let ub = document.getElementById(id4);--}}
            {{--a.style.display = ((a.style.display!=='none') ? 'none' : 'block');--}}
            {{--u.style.display = ((u.style.display!=='none') ? 'none' : 'block');--}}
            {{--ab.style.display = ((ab.style.display!=='none') ? 'none' : 'block');--}}
            {{--ub.style.display = ((ub.style.display!=='none') ? 'none' : 'block');--}}
        {{--}--}}

        {{--function confirmDel() {--}}
            {{--let del = confirm('Are you sure to delete this Category?');--}}
            {{--if(del)--}}
                {{--return true;--}}
            {{--else--}}
                {{--return false;--}}
        {{--}--}}

{{--@endsection--}}
