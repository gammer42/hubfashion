@extends('layouts.admin')
@section('navbar')
    <a class="navbar-brand bold" href="#">Product update</a>
@endsection
@section('content')

    <div class="wrapper ">

        @include('admin.partial.sidebar')

        <div class="main-panel">
            <!-- Navbar -->
        @include('admin.partial.nav')


        <!--  User Details -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title bold">Update Product</h4>
                                    <p class="card-category">Update Product in Here...</p>
                                </div>
                                <div class="card-body" >
                                    <form action="{{route('products.update', $products->id)}}" method="POST" name="edit_product_form">
                                        @csrf
                                        @method('PUT')

                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10">
                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <span><br/></span></div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Product Name</label>
                                                    <input type="text" id="product_name" class="form-control" name="name" value="{{ $products->name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Category</label>
                                                    <select style=" color:#ffffff;" class="form-control" name="category" data-placeholder="Category">
                                                        @foreach($categories as $parent)
                                                            <option class="bold" style="background-color:#0e0e40;" value="{{ $parent->id }}">{{ $parent->name }}</option>
                                                            @if($parent->children->count())
                                                                @foreach($parent->children as $child)
                                                                    <option style="background-color:#153140;" value="{{ $child->id }}"><span> {{ $child->name }}</span></option>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Brands</label>
                                                    <select style=" color:#ffffff;" class="form-control" name="brand" data-placeholder="Brands">
                                                        @foreach($brands as $brand)
                                                            <option class="bold" style="background-color:#0e0e40;" value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Slug</label>
                                                    <input type="text" id="slug_name" class="form-control" name="slug" value="{{ $products->slug }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Ideals</label>
                                                    <select style="color:#888D9B; color:#ffffff;" class="form-control" name="ideal" data-placeholder="Ideal">
                                                        <option class="bold" style="background-color:#0e0e40;" value="Mens" >Mens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Womens" >Womens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Boys" >Boys</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Girls" >Girls</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Childrens">Childrens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Infant">Infant</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Size</label>
                                                    <select style="color:#888D9B; color:#ffffff;" class="form-control" name="size" data-placeholder="Ideal">
                                                        <option class="bold" style="background-color:#0e0e40;" value="XS">Extra Small (XS)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="S">Small (S)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="M">Medium (M)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="L">Long (L)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XL">Extra Long (XL)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XXL">Double Extra Long (XXL)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XXXL">Very Long (XXXL)</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Fabric</label>
                                                    <input type="text" id="fabric" class="form-control" name="fabric" value="{{ $products->fabric }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Color</label>
                                                    <input type="text" id="color" class="form-control" name="color" value="{{ $products->color }}">
                                                </div>
                                            </div>
                                        <!-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Image') }}</label>
                                                    <input type="text" id="price" class="form-control" name="price" value="{{old('image')}}">
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Price TK</label>
                                                    <input type="number" id="price" class="form-control" name="price" value="{{ $products->price }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Discount (%)</label>
                                                    <input type="number" id="price" class="form-control" name="discount" value="{{ $products->discount }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Description</label>
                                                    <textarea type="text" id="description" class="form-control" name="description" rows="5">{{ $products->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary pull-right" type="reset" ><a href="{{route('brands.index')}}" >Cancel</a></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            @include('admin.partial.footer')
        </div>
    </div>

    {{--@include('admin.partial.plugin')--}}

    @include('admin.partial.script')
@endsection

@section('script')
    <script>
        const x = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = '&copy; ' + x + date.innerHTML;

        function checkPermission() {
            let checkBox = document.getElementById("checkBox");
            let permissionBox = document.getElementById("permissionBox");
            if (checkBox.checked !== true){
                permissionBox.style.display = "block";
            } else {
                permissionBox.style.display = "none";
            }
        }

        function toggle(source) {
            let checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (let i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] !== source)
                    checkboxes[i].checked = source.checked;
            }
        }

        $("#product_name").keyup(function(){
            let text = $(this).val();
            // let parslug = $('#parent').find(":selected").text();
            // // let text = $('#category_name').val();
            // let slug = custom_str_concate(parslug,text);
            // slug = slug.trim();
            slug = text.trim();
            slug = slug.toLowerCase().replace(/ /g, '-').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
            $("#slug_name").val(slug);
        });

        function custom_str_concate(val1,val2){
            if(val1 === "/")
                return '_'+val1.concat(val2);
            else
                return val1+'-'+val2;
        }
        $(document).ready(function(){
            $("select#parent").change(function(){
                let val1 = $('#parent').find(":selected").text();
                let val2 = $('#category_name').val();
                let slug = custom_str_concate(val1,val2);
                slug = slug.trim();
                slug = slug.toLowerCase().replace(/ /g, '_').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
                $("#slug_name").val(slug);
            });
        });
        document.forms['edit_product_form'].elements['size'].value = "{{ $products->size }}";
        document.forms['edit_product_form'].elements['brand'].value = "{{ $products->brand_id }}";
        document.forms['edit_product_form'].elements['ideal'].value = "{{ $products->ideal }}";
        document.forms['edit_product_form'].elements['category'].value = "{{ $products->category_id }}";
    </script>



@endsection
