@extends('layouts.admin')

@section('head')
@endsection

@section('navbar')
    <a class="navbar-brand" href="#" title="View Customer Users table">Products</a>
@endsection

@section('content')

    <div class="wrapper ">

        @include('admin.partial.sidebar')


        <div class="main-panel">
            <!-- Navbar -->
        @include('admin.partial.nav')
        <!-- End Navbar -->

            <!-- User List Table  -->

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12" id="admin" style="display: block;">
                            <div class="card card-plain">
                                <div class="card-header card-header-success">
                                    @if(session('message'))
                                        <h3 style="text-align: center; color:snow; font-weight: bold;" class="card-title mt-0">{{session('message')}}</h3>
                                    @endif
                                </div>
                                <div class="card-header card-header-primary">
                                    <div class="col-md-4 "style="float:left;">
                                        <h4 class="card-title mt-0">Products</h4>
                                        <p class="card-category">Products Table</p>
                                    </div>
                                    <div class="col-md-4" style="float:left;">
                                        <h4 class="card-title mt-0"><a  class="nav-link navbar-brand" href="{{route('products.create')}}"  title="Create New Product">Create New Product</a></h4>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="table_id">
                                            <!-- <thead class=" text-primary"> -->
                                            <thead class=" text-primary">
                                            <th style="width: 50px;">
                                                SL No
                                            </th>
                                            <th style="width: 100px;">
                                                Name
                                            </th>
                                            <th style="width: 100px;">
                                                Slug
                                            </th>
                                            <th style="width: 80px;">
                                                Ideal
                                            </th>
                                            <th style="width: 80px;">
                                                Size
                                            </th>
                                            <th style="width: 80px;">
                                                Fabric
                                            </th>
                                            <th style="width: 80px;">
                                                Color
                                            </th>
                                            <th style="width: 80px;">
                                                Price
                                            </th>
                                            <th style="width: 300px;">
                                                Description
                                            </th>
                                            <th class="action-btn" style="width: 120px;">
                                                Action
                                            </th>
                                            </thead>
                                            <tbody>
                                            <?php $i=0; ?>
                                            @foreach($products as $product)
                                                <tr style="background:#202940;" class="table-row">
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-success">{{ ++$i }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-other">{{ $product->name }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-success">{{ $product->slug }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-other">{{ $product->ideal }}</span>
                                                    </td>
                                                    <td class="white">
                                                        @if($product->size == 'XS')
                                                            <span class=" label spacial mod-btn-success">Extra Small (XS)</span>
                                                        @elseif($product->size == 'S')
                                                            <span class=" label spacial mod-btn-success"> Small (S)</span>
                                                        @elseif($product->size == 'M')
                                                            <span class=" label spacial mod-btn-success">Medium (M)</span>
                                                        @elseif($product->size == 'L')
                                                            <span class=" label spacial mod-btn-success">Long (L)</span>
                                                        @elseif($product->size == 'XL')
                                                            <span class=" label spacial mod-btn-success">Extra Long (XL)</span>
                                                        @elseif($product->size == 'XXL')
                                                            <span class=" label spacial mod-btn-success">Double Extra Long (XXL)</span>
                                                        @elseif($product->size == 'XXXL')
                                                            <span class=" label spacial mod-btn-success">Very Long (XXXL)</span>
                                                        @endif
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-other">{{ $product->fabric }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-success">{{ $product->color }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <span class=" label spacial mod-btn-other">{{ $product->price }}</span>
                                                    </td>
                                                    <td class="white">
                                                        <?php
                                                        $res = strip_tags($product->description);
                                                        echo substr($res,0,100);
                                                        echo (strlen($res)>200)? "...." : "";
                                                        ?>
                                                    </td>
                                                    <td class="center white">
                                                        <a class="m-btn btn {{ $product->status == 1? "btn-success":"btn-warning" }}" href="{{route('products.show',$product->id)}}">
                                                            <i class="material-icons">search</i>
                                                        </a>
                                                        <a class="m-btn btn btn-info" href="{{route('products.edit',$product->id)}}">
                                                            <i class="material-icons">settings</i>
                                                        </a>
                                                        <form style="display: inline !important;" action="{{route('products.destroy', $product->id)}}" method="POST">
                                                            @CSRF
                                                            @METHOD('DELETE')
                                                            <button type="submit" class="m-btn btn btn-danger" title="Delete Product" onclick="return confirmDel()">
                                                                <i class="material-icons">delete</i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  Footer  -->

        @include('admin.partial.footer')

    </div>
    </div>


@endsection

@section('script')

    <script>
        const x = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = '&copy; ' + x + date.innerHTML;
    </script>

    <script>
        function toggle_visibility(id1,id2,id3,id4) {
            let a = document.getElementById(id1);
            let u = document.getElementById(id2);
            let ab = document.getElementById(id3);
            let ub = document.getElementById(id4);
            a.style.display = ((a.style.display!=='none') ? 'none' : 'block');
            u.style.display = ((u.style.display!=='none') ? 'none' : 'block');
            ab.style.display = ((ab.style.display!=='none') ? 'none' : 'block');
            ub.style.display = ((ub.style.display!=='none') ? 'none' : 'block');
        }

        function confirmDel() {
            let del = confirm('Are you sure to delete this Category?');
            if(del)
                return true;
            else
                return false;
        }
    </script>

@endsection
