@extends('layouts.admin')
@section('navbar')
    <a class="navbar-brand bold" href="#">Product Added by Authority</a>
@endsection
@section('content')

    <div class="wrapper ">

        @include('admin.partial.sidebar')

        <div class="main-panel">
            <!-- Navbar -->
        @include('admin.partial.nav')


        <!--  User Details -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title bold">{{ __('Add Product') }}</h4>
                                    <p class="card-category">{{ __('Authority can Add Product in Here...') }}</p>
                                </div>
                                <div class="card-body" >
                                    <form action="{{route('products.store')}}"  enctype="multipart/form-data"  method="POST">
                                        @CSRF
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10">
                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <span><br/></span></div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">{{ __('Product Name') }}</label>
                                                    <input type="text" id="product_name" class="form-control" name="name" value="{{old('name')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">{{ __('Category') }}</label>
                                                    <select style=" color:#ffffff;" class="form-control selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" name="category" data-placeholder="Category">
                                                        <option class="bold" style="background-color:#0e0e40;" value="" disabled selected >Select Category</option>
                                                        @foreach($categories as $parent)
                                                            <option class="bold" style="background-color:#0e0e40;" value="{{ $parent->id }}">{{ $parent->name }}</option>
                                                            @if($parent->children->count())
                                                                @foreach($parent->children as $child)
                                                                    <option style="background-color:#153140;" value="{{ $child->id }}"><span> {{ $child->name }}</span></option>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">Brands</label>
                                                    <select style=" color:#ffffff;" class="form-control selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" name="brand" data-placeholder="Brands">
                                                        <option class="bold" style="background-color:#0e0e40;" value="" disabled selected >Select Brands</option>
                                                    @foreach($brands as $brand)
                                                            <option class="bold" style="background-color:#0e0e40;" value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Slug') }}</label>
                                                    <input type="text" id="slug_name" class="form-control" name="slug" value="{{old('slug')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">{{ __('Ideals') }}</label>
                                                    <select style="color:#888D9B; color:#ffffff;" class="form-control" name="ideal" data-placeholder="Ideal">
                                                        <option class="bold" style="background-color:#0e0e40;" value="">Ideals For</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Mens">Mens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Womens">Womens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Boys">Boys</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Girls">Girls</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Childrens">Childrens</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="Infant">Infant</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating bold">{{ __('Size') }}</label>
                                                    <select style="color:#888D9B; color:#ffffff;" class="form-control" name="size" data-placeholder="Ideal">
                                                        <option class="bold" style="background-color:#0e0e40;" value=""></option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XS">Extra Small (XS)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="S" >Small (S)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="M">Medium (M)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="L">Long (L)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XL">Extra Long (XL)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XXL">Double Extra Long (XXL)</option>
                                                        <option class="bold" style="background-color:#0e0e40;" value="XXXL">Very Long (XXXL)</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Fabric</label>
                                                    <input type="text" id="fabric" class="form-control" name="fabric" value="{{old('fabric')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-file-upload">
                                                    <span class="btn btn-raised btn-round btn-default btn-file"><input type="file" class="" id="files" name="image[]" placeholder="Select" multiple/>Select Image</span>
                                                </div>
                                                {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
                                                {{--<div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>--}}
                                                {{--<div>--}}
                                                {{--<span class="btn btn-raised btn-round btn-default btn-file">--}}
                                                {{--<span>Select image</span>--}}
                                                {{--<span class="fileinput-exists"  data-dismiss="fileinput">Change</span>--}}
                                                {{--<input type="file" id="gallery-photo-add" name="image[]" multiple/>--}}
                                                {{--</span>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Color</label>
                                                    <input type="text" id="color" class="form-control" name="color" value="{{old('color')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Quantity</label>
                                                    <input type="number" id="quantity" class="form-control" name="quantity" value="{{old('quantity')}}">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Price TK</label>
                                                    <input type="number" id="price" class="form-control" name="price" value="{{old('price')}}">
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Discount (%)</label>
                                                    <input type="number" id="discount" class="form-control" name="discount" value="{{old('discount')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Description</label>
                                                    <textarea type="text" id="description" class="form-control" name="description" rows="5">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary pull-right">Create Products</button>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary pull-right" type="reset" ><a href="{{route('products.index')}}" >Cancel</a></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div id="image1" class="text-center">
                                <div id="image2" class="fileinput-preview fileinput-exists thumbnail img-raised">
                                    <div class="photoinfo" id="photoinfo">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Footer -->
            @include('admin.partial.footer')
        </div>
    </div>

    {{--@include('admin.partial.plugin')--}}


    @include('admin.partial.script')
@endsection

@section('script')
    <script>
        const x = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = '&copy; ' + x + date.innerHTML;

        function checkPermission() {
            let checkBox = document.getElementById("checkBox");
            let permissionBox = document.getElementById("permissionBox");
            if (checkBox.checked !== true){
                permissionBox.style.display = "block";
            } else {
                permissionBox.style.display = "none";
            }
        }

        function toggle(source) {
            let checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (let i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] !== source)
                    checkboxes[i].checked = source.checked;
            }
        }

        $("#product_name").keyup(function(){
            let text = $(this).val();
            // let parslug = $('#parent').find(":selected").text();
            // // let text = $('#category_name').val();
            // let slug = custom_str_concate(parslug,text);
            // slug = slug.trim();
            slug = text.trim();
            slug = slug.toLowerCase().replace(/ /g, '-').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
            $("#slug_name").val(slug);
        });

        function custom_str_concate(val1,val2){
            if(val1 === "/")
                return '_'+val1.concat(val2);
            else
                return val1+'-'+val2;
        }
        $(document).ready(function(){
                $("select#parent").change(function(){
                let val1 = $('#parent').find(":selected").text();
                let val2 = $('#category_name').val();
                let slug = custom_str_concate(val1,val2);
                    slug = slug.trim();
                    slug = slug.toLowerCase().replace(/ /g, '_').replace(/^\s+|\s+$[^\w-]+ /g, '').replace(/_->_/gm,"-");
                $("#slug_name").val(slug);
            });
        });

        let storedFiles = []
        function handleFileSelect(evt) {
            let files = evt.target.files;
            let counter = 0;
            for (let i = 0, f; f = files[i]; i++,counter++) {

                if (!f.type.match('image.*')) {
                    return;
                }
                storedFiles.push(f);
                let reader = new FileReader();
                let rem = 'Remove Image';
                reader.onload = (function(theFile) {
                    let count = counter;
                    return function(e) {
                        let span = document.createElement('span');
                        span.innerHTML = ['<img style="width:300px; border-width:3px; border-style:solid; border-color:#000; margin:8px;" src="', e.target.result, '" title="', escape(theFile.name), '" id="img" class="selFile"/>', '<button type="submit" style="top: -40px !important; padding: 5px 10px !important; margin: 0px 30px !important;" class="btn btn-danger btn-round pull-right dl btn-sm" title="Remove Image" id="remove" onclick="confirmDel()">',rem,'</button>'].join('');
                        document.getElementById('photoinfo').insertBefore(span, null);
                    };
                })(f);

                reader.readAsDataURL(f);
            }

        }
        document.getElementById('files').addEventListener('change', handleFileSelect, false);


    function removeFile(e) {
        let file = $(this).data("file");
        for (let i = 0; i < storedFiles.length; i++) {
            document.write(storedFiles[i].name);
        }
        // $(this).parent().remove()
        // file = null;
    }

    function confirmDel() {
        let del = confirm('Are you sure to remove?');
        if(del) {
            let nameImg = $("#img").attr('title')
            // $("#remove").parent().remove();

            for (let i = 0; i < storedFiles.length; i++) {
                document.write('<br>');
                if(storedFiles[i].name === nameImg){
                    storedFiles[i].remove();
                }
                document.write(storedFiles[i].name);
                document.write('<br>');

            }
        }
        else
            return false;
    }

    </script>

@endsection
