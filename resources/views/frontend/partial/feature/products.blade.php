<section class="tabs_pro py-md-5 pt-sm-3 pb-5">
    <h5 class="head_agileinfo text-center text-capitalize pb-5">
        <span>s</span>mart clothing</h5>
    <div class="tabs tabs-style-line pt-md-5">
        <nav class="container">
            <ul id="clothing-nav" class="nav nav-tabs tabs-style-line" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#women" id="women-tab" role="tab" data-toggle="tab" aria-controls="women" aria-expanded="true">Women's Fashion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#men" role="tab" id="men-tab" data-toggle="tab" aria-controls="men">Men's Fashion
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#girl" role="tab" id="girl-tab" data-toggle="tab" aria-controls="girl">Girl's Fashion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#boy" role="tab" id="boy-tab" data-toggle="tab" aria-controls="boy">Boy's Fashion</a>
                </li>
            </ul>
        </nav>
        <!-- Content Panel -->
        <div id="clothing-nav-content" class="tab-content py-sm-5">
            <div role="tabpanel" class="tab-pane fade show active" id="women" aria-labelledby="women-tab">
                <div id="owl-demo" class="owl-carousel text-center">
                    @foreach($womens_products as $women)
                    <div class="item">
                        <!-- card -->
                        <div class="card product-men p-3">
                            <div class="men-thumb-item">
                                <img src="{{ url('/images/products', $women->photos[0]->org_image) }}" alt="img" class="card-img-top">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="{{ route('women'), $women->slug }}" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- card body -->
                            <div class="card-body  py-3 px-2">
                                <h5 class="card-title text-capitalize">{{ $women->name }}</h5>
                                <div class="card-text d-flex justify-content-between">
                                    <p class="text-dark font-weight-bold">
                                        <?php
                                            $discount = 100-$women->discount;
                                            $offer = ($discount*$women->price)/100;
                                            echo $offer;
                                        ?>
                                    </p>
                                    <p class="line-through">{{ $women->price }} Tk.</p>
                                </div>
                            </div>
                            <!-- card footer -->
                            <div class="card-footer d-flex justify-content-end">
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="add" value="1">
                                    <input type="hidden" name="hub_item" value="Self Design Women's Tunic">
                                    <input type="hidden" name="amount" value="28.00">
                                    <button type="submit" class="hub-cart phub-cart btn">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" data-toggle="modal" data-target="#myModal1"></a>
                                </form>
                            </div>
                        </div>
                        <!-- //card -->
                    </div>
                    @endforeach
                    <div class="item">
                        <div class="product-men p-3 text-center">
                            <img src="{{asset('frontend/images/p2.png')}}" class="img-responsive" alt="" />
                            <a href="women.html" class="btn btn-lg bg-info text-white">view more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="men" aria-labelledby="men-tab">
                <div id="owl-demo1" class="owl-carousel text-center">
                    @foreach($mens_products as $mens)
                    <div class="item">
                        <!-- card -->
                        <div class="card product-men p-3">
                            <div class="men-thumb-item">
                                <img src="{{ url('/images/products', $mens->photos[0]->org_image) }}" alt="img" class="card-img-top">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="{{ route('men', $mens->slug) }}" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- card body -->
                            <div class="card-body  py-3 px-2">
                                <h5 class="card-title text-capitalize">{{ $mens->name }}</h5>
                                <div class="card-text d-flex justify-content-between">
                                    <p class="text-dark font-weight-bold">
                                        <?php
                                        $discount = 100-$mens->discount;
                                        $offer = ($discount*$mens->price)/100;
                                        echo $offer;
                                        ?> Tk.
                                    </p>
                                    <p class="line-through">{{ $mens->price }} Tk.</p>
                                </div>
                            </div>
                            <!-- card footer -->
                            <div class="card-footer d-flex justify-content-end">
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="add" value="1">
                                    <input type="hidden" name="hub_item" value="Solid Formal Black Shirt">
                                    <input type="hidden" name="amount" value="40.00">
                                    <button type="submit" class="hub-cart phub-cart btn">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" data-toggle="modal" data-target="#myModal1"></a>
                                </form>
                            </div>
                        </div>
                        <!-- //card -->
                    </div>
                    @endforeach
                    <div class="item">
                        <div class="product-men p-3 text-center">
                            <img src="{{asset('frontend/images/p2.png')}}" class="img-responsive" alt="">
                            <a href="men.html" class="btn btn-lg bg-info text-white">view more</a>
                        </div>
                        <!-- //card -->
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="girl" aria-labelledby="girl-tab">
                <div id="owl-demo2" class="owl-carousel text-center">
                    @foreach($girls_products as $girls)
                    <div class="item">
                        <!-- card -->
                        <div class="card product-men p-3">
                            <div class="men-thumb-item">
                                <img src="{{url('/images/products/', $girls->photos[0]->org_image)}}" alt="img" class="card-img-top">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="{{ route('girl', $girls->slug) }}" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- card body -->
                            <div class="card-body  py-3 px-2">
                                <h5 class="card-title text-capitalize">{{ $girls->name }}</h5>
                                <div class="card-text d-flex justify-content-between">
                                    <p class="text-dark font-weight-bold">
                                        <?php
                                        $discount = 100-$mens->discount;
                                        $offer = ($discount*$mens->price)/100;
                                        echo $offer;
                                        ?> Tk.
                                    </p>
                                    <p class="line-through">{{ $mens->price }} Tk.</p>
                                </div>
                            </div>
                            <!-- card footer -->
                            <div class="card-footer d-flex justify-content-end">
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="add" value="1">
                                    <input type="hidden" name="hub_item" value="Midi/Knee Length Party Dress">
                                    <input type="hidden" name="amount" value="18.00">
                                    <button type="submit" class="hub-cart phub-cart btn">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" data-toggle="modal" data-target="#myModal1"></a>
                                </form>
                            </div>
                        </div>
                        <!-- //card -->
                    </div>
                    @endforeach
                    <div class="item">
                        <div class="product-men p-3 text-center">
                            <img src="{{asset('frontend/images/p2.png')}}" class="img-responsive" alt="" />
                            <a href="girls.html" class="btn btn-lg bg-info text-white">view more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="boy" aria-labelledby="boy-tab">
                <div id="owl-demo3" class="owl-carousel text-center">
                    @foreach($boys_products as $boys)
                    <div class="item">
                        <!-- card -->
                        <div class="card product-men p-3">
                            <div class="men-thumb-item">
                                <img src="{{ url('/images/products/', $boys->photos[0]->org_image) }}" alt="img" class="card-img-top">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="{{ route('girl', $boys->slug) }}" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                            </div>
                            <!-- card body -->
                            <div class="card-body  py-3 px-2">
                                <h5 class="card-title text-capitalize">{{ $boys->name }}</h5>
                                <div class="card-text d-flex justify-content-between">
                                    <p class="text-dark font-weight-bold">
                                        <?php
                                        $discount = 100-$mens->discount;
                                        $offer = ($discount*$mens->price)/100;
                                        echo $offer;
                                        ?> Tk.
                                    </p>
                                    <p class="line-through">{{ $boys->price }} Tk.</p>
                                </div>
                            </div>
                            <!-- card footer -->
                            <div class="card-footer d-flex justify-content-end">
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="add" value="1">
                                    <input type="hidden" name="hub_item" value="Shirt, Waistcoat and Pant Set">
                                    <input type="hidden" name="amount" value="21.00">
                                    <button type="submit" class="hub-cart phub-cart btn">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" data-toggle="modal" data-target="#myModal1"></a>
                                </form>
                            </div>
                        </div>
                        <!-- //card -->
                    </div>
                    @endforeach
                    <div class="item">
                        <div class="product-men p-3 text-center">
                            <img src="{{asset('frontend/images/p2.png')}}" class="img-responsive" alt="" />
                            <a href="boys.html" class="btn btn-lg bg-info text-white">view more</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
