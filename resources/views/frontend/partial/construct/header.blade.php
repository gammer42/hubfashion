<header>
    <div class="container">
        <!-- top nav -->
        <nav class="top_nav d-flex pt-3 pb-1">
            <!-- logo -->
            <h1>
                <a class="navbar-brand" href="{{route('index')}}">fh
                </a>
            </h1>
            <!-- //logo -->
            <div class="w3ls_right_nav ml-auto d-flex">
                @if(session('message'))
                    <h3 style="text-align: center; color:#8b6812; font-weight: bold;" class="card-title">{{session('message')}}</h3>
                @endif
            </div>
            <div class="w3ls_right_nav ml-auto d-flex">
                <!-- search form -->
                <form class="nav-search form-inline my-0 form-control" action="#" method="post">
                    <label>
                        <select class="form-control input-lg" name="category">
                            <option value="all">Search our store</option>
                            <optgroup label="Mens">
                                <option value="T-Shirts">T-Shirts</option>
                                <option value="coats-jackets">Coats & Jackets</option>
                                <option value="Shirts">Shirts</option>
                                <option value="Suits & Blazers">Suits & Blazers</option>
                                <option value="Jackets">Jackets</option>
                                <option value="Sweat Shirts">Trousers</option>
                            </optgroup>
                            <optgroup label="Womens">
                                <option value="Dresses">Dresses</option>
                                <option value="T-shirts">T-shirts</option>
                                <option value="skirts">Skirts</option>
                                <option value="jeans">Jeans</option>
                                <option value="Tunics">Tunics</option>
                            </optgroup>
                            <optgroup label="Girls">
                                <option value="Dresses">Dresses</option>
                                <option value="T-shirts">T-shirts</option>
                                <option value="skirts">Skirts</option>
                                <option value="jeans">Jeans</option>
                                <option value="Tops">Tops</option>
                            </optgroup>
                            <optgroup label="Boys">
                                <option value="T-Shirts">T-Shirts</option>
                                <option value="coats-jackets">Coats & Jackets</option>
                                <option value="Shirts">Shirts</option>
                                <option value="Suits & Blazers">Suits & Blazers</option>
                                <option value="Jackets">Jackets</option>
                                <option value="Sweat Shirts">Sweat Shirts</option>
                            </optgroup>
                        </select>
                    </label>
                    <input class="btn btn-outline-secondary  ml-3 my-sm-0" type="submit" value="Search">
                </form>
                <!-- search form End -->
                <div class="nav-icon d-flex">
                    <!-- shopping cart -->
                    <div class="cart-mainf">
                        <div class="hubcart hubcart2 cart cart box_1">
                            <form action="{{ route('checkout') }}" method="post">
                                @csrf
                                <input type="hidden" name="cmd" value="_cart">
                                <input type="hidden" name="display" value="1">
                                <button class="btn top_hub_cart mt-1" type="submit" name="submit" value="" title="Cart">
                                    <i class="fas fa-shopping-bag"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    <?php $user = Session::get('user'); $id = Session::get('id'); ?>
                    <li class="dropdown" style="display:flex;">
                        @if($user)
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user"></i> {{$user}}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-menu-title">
                                    <span>Account Settings</span>
                                </li> <br>
                                <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li> <br>
                                <li><a href="{{Route('logout')}}"><i class="halflings-icon off"></i>Logout</a></li>
                            </ul>
                        @else
                            <button class="btn top_hub_cart mt-1" title="Login Now">
                                <a href="{{Route('user-login')}}" title="Login Now"><i class="fas fa-user" title="Login Now"></i></a>
                            </button>
                            @if (Route::has('register'))
                                <button class="btn top_hub_cart mt-1" title="Register Now">
                                    <a href="{{ route('register') }}" title="Register Now"><i class="fas fa-user-plus" title="Register Now"></i></a>
                                </button>
                            @endif
                        @endif
                    </li>
                    <!-- sigin and sign up -->
                </div>
            </div>
        </nav>
        <!-- //top nav -->
        <!-- bottom nav -->
        <nav class="navbar navbar-expand-lg navbar-light justify-content-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto text-center">
                    <li class="nav-item">
                        <a class="nav-link  active" href="{{route('index')}}">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown has-mega-menu" style="position:static;">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Men's clothing</a>
                        <div class="dropdown-menu" style="width:100%;">
                            <div class=" container">
                                <div class="col-md-12">
                                    <div class="row w3_kids">
                                        <a class="dropdown-item" href="{{route('mens',1)}}">
                                            <span class="bg-light">Mens All category</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php $shaped_ar = array_chunk($mens,5) ?>
                                    @foreach($shaped_ar as $row)
                                        <div class="col-md-4">
                                            @foreach($row as $item)
                                                <a class="dropdown-item" href="{{route('mens', $item['id'])}}">{{ $item['name'] }}</a>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown has-mega-menu" style="position:static;">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Women's clothing</a>
                        <div class="dropdown-menu" style="width:100%">
                            <div class="px-0 container">
                                <div class="col-md-12">
                                    <div class="row w3_kids">
                                        <a class="dropdown-item" href="{{route('womens',2)}}">
                                            <span class="bg-light">Women's All category</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php $shaped_ar = array_chunk($womens,5) ?>
                                    @foreach($shaped_ar as $row)
                                        <div class="col-md-4">
                                        @foreach($row as $item)
                                            <a class="dropdown-item" href="{{route('womens', $item['id'])}}">{{ $item['name'] }}</a>
                                        @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown has-mega-menu" style="position:static;">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kids Clothing</a>
                        <div class="dropdown-menu" style="width:100%;">
                            <div class="container">
                                <div class="col-md-12" style=" display: flex;">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="row w3_kids">
                                                <a class="dropdown-item" href="{{route('boys',4)}}">
                                                    <span class="bg-light">Boy's Clothings</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row w3_kids  py-3">
                                            <?php $shaped_ar = array_chunk($boys,5) ?>
                                            @foreach($shaped_ar as $row)
                                                <div class="col-md-6">
                                                    @foreach($row as $item)
                                                        <a class="dropdown-item" href="{{route('boys', $item['id'])}}">{{ $item['name'] }}</a>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="row w3_kids">
                                                <a class="dropdown-item" href="{{route('girls',3)}}">
                                                    <span class="bg-light">Girl's Clothings</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row w3_kids  py-3">
                                            <?php $shaped_ar = array_chunk($girls,5) ?>
                                            @foreach($shaped_ar as $row)
                                                <div class="col-md-6">
                                                    @foreach($row as $item)
                                                        <a class="dropdown-item" href="{{route('girls', $item['id'])}}">{{ $item['name'] }}</a>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('about')}}">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('blog')}}">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('contact')}}">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- //bottom nav -->
    </div>
    <!-- //header container -->
</header>
