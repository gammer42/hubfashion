@extends('layouts.master')

@section('content')
    <!-- banner -->
    @include('frontend.partial.feature.banner')
    <!-- //banner -->
    <!--services-->
    @include('frontend.partial.feature.service')
    <!-- //services-->
    <!-- about -->
    @include('frontend.partial.feature.about')
    <!-- //about -->
    <!-- product tabs -->
    @include('frontend.partial.feature.products')
    <!-- //product tabs -->
    <!-- insta posts -->
    @include('frontend.partial.feature.insta')
    <!-- //insta posts -->
@endsection
<!-- footer -->
<!-- //footer -->
<!-- sign up Modal -->
@section('modal')
    @include('frontend.partial.construct.modal')
@endsection
<!-- signin Modal -->
<!-- js -->
