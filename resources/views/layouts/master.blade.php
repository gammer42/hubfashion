<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('frontend.partial.construct.head')

<body>
    @include('frontend.partial.construct.header')

    @yield('content')

    @include('frontend.partial.construct.footer')

    {{--@yield('modal')--}}

    @include('frontend.partial.construct.script')

<script>
    $(document).ready(function (){

        var form = $("#staplesbmincart");
        $("button.hub-cart").on('click' , function (){
            setTimeout(function(){
                $('.form_cart').append('<input type="hidden" name="_token" value="{{ csrf_token() }}">');
            },20);
            console.log('test');
        });
        $('.form_cart').append('<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">');
    });
    function refreshToken(){
        $.get('refresh-csrf').done(function(data){
            csrfToken = data; // the new token
        });
    }
    setInterval(refreshToken, 3600000); // 1 hour
</script>

</body>

</html>
